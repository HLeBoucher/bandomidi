
/*
 * Here are the way the keys of my 60's 
 * chromatic Fratelli-Crosio are linked
 * to BandoMIDI
 */

// Notes (right hand)
const int MIDI_NOTE_R1_0 = pitchE4b;
const int MIDI_NOTE_R1_1 = pitchE4;
const int MIDI_NOTE_R1_2 = pitchF4;
const int MIDI_NOTE_R1_3 = pitchA4;
const int MIDI_NOTE_R1_4 = pitchB4b;
const int MIDI_NOTE_R1_5 = pitchB4;

const int MIDI_NOTE_R2_0 = pitchE5b;
const int MIDI_NOTE_R2_1 = pitchE5;
const int MIDI_NOTE_R2_2 = pitchF5;
const int MIDI_NOTE_R2_3 = pitchA5;
const int MIDI_NOTE_R2_4 = pitchB5b;
const int MIDI_NOTE_R2_5 = pitchB5;
const int MIDI_NOTE_R2_6 = pitchE6b;

const int MIDI_NOTE_R3_1 = pitchE6;
const int MIDI_NOTE_R3_2 = pitchG6;
const int MIDI_NOTE_R3_3 = pitchE6b;
const int MIDI_NOTE_R3_4 = pitchG6b;
const int MIDI_NOTE_R3_5 = pitchD6;

const int MIDI_NOTE_R4_0 = pitchF6;
const int MIDI_NOTE_R4_1 = pitchD4b;
const int MIDI_NOTE_R4_2 = pitchB3b;
const int MIDI_NOTE_R4_3 = pitchC4;
const int MIDI_NOTE_R4_4 = pitchA3;
const int MIDI_NOTE_R4_5 = pitchB3;

const int MIDI_NOTE_R5_0 = pitchD6b;
const int MIDI_NOTE_R5_1 = pitchC6;
const int MIDI_NOTE_R5_2 = pitchA5b;
const int MIDI_NOTE_R5_3 = pitchG5;
const int MIDI_NOTE_R5_4 = pitchG5b;
const int MIDI_NOTE_R5_5 = pitchD5;

const int MIDI_NOTE_R6_0 = pitchD5b;
const int MIDI_NOTE_R6_1 = pitchC5;
const int MIDI_NOTE_R6_2 = pitchA4b;
const int MIDI_NOTE_R6_3 = pitchG4;
const int MIDI_NOTE_R6_4 = pitchG4b;
const int MIDI_NOTE_R6_5 = pitchD4;

