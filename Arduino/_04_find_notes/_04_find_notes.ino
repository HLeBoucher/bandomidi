
/*
 * Listing MIDI port:
 *  - aconnect -l
 * Monitoring events on a port:
 *  - aseqdump -p 24
 * Starting the synthetiser as a deamon (-K -2 for B flat):
 *  - timidity -iA -B2,8 -Os1l -s 44100 -K -2
 * Linking input with output
 *  - aconnect 24 128
 */

/* Dependencies */
#include <Wire.h>    // Required for I2C communication
#include <PCF8574.h>
//#include <PCint.h>
//#include <frequencyToNote.h>
//#include <pitchToNote.h>
#include <MIDIUSB.h>
//#include <pitchToFrequency.h>
#include <avr/sleep.h>


#define USE_INT
//#define ADDR_A000 // Left hand
//#define ADDR_A001
//#define ADDR_A010
//#define ADDR_A011
//#define ADDR_A100
//#define ADDR_A101
#define ADDR_000 // Right hand
#define ADDR_001
#define ADDR_010
#define ADDR_011
#define ADDR_100
#define ADDR_101

#define DISPLAY_BIT
//#define DISPLAY_ADDR


// Constants
const byte interruptPin = 0; // Rx Pin / INT2

// PCF8574 instances
PCF8574 expanderR1; // inverted
PCF8574 expanderR2; // inverted
PCF8574 expanderR3;
PCF8574 expanderR4;
PCF8574 expanderR5;
PCF8574 expanderR6;
PCF8574 expanderL1;
PCF8574 expanderL2;
PCF8574 expanderL3; // inverted
PCF8574 expanderL4; // inverted
PCF8574 expanderL5;
PCF8574 expanderL6;
bool newNote;

void interruption()
{
//  Serial.print("!");
  newNote = true;
}

void setup()
{
    pinMode(interruptPin, INPUT_PULLUP);
    newNote = false;
    attachInterrupt(digitalPinToInterrupt(interruptPin), interruption, FALLING);
  
    // Setup serial for debug
    Serial.begin(115200);
  
    delay(1000);
    Serial.println("In0");
    delay(1000);
    Serial.println("In1");
    
    // Start I2C bus and PCF8574(A) instances
#ifdef ADDR_A000
    expanderL1.begin(0x20); // Address 000
    Serial.print("000 ");
#endif ADDR_A000
#ifdef ADDR_A001
    expanderL2.begin(0x21); // Address 001 // not working
    Serial.print("001 ");
#endif ADDR_A001
#ifdef ADDR_A010
    expanderL3.begin(0x22); // Address 010
    Serial.print("010 ");
#endif ADDR_A010
#ifdef ADDR_A011
    expanderL4.begin(0x23); // Address 011
    Serial.print("011 ");
#endif ADDR_A011
#ifdef ADDR_A100
    expanderL5.begin(0x24); // Address 100 // not working
    Serial.print("100 ");
#endif ADDR_A100
#ifdef ADDR_A101
    expanderL6.begin(0x25); // Address 101
    Serial.print("101 ");
#endif ADDR_A101


#ifdef ADDR_000
    expanderR1.begin(0x38); // Address A-000
    Serial.print("A000 ");
#endif ADDR_000
#ifdef ADDR_001
    expanderR2.begin(0x39); // Address A-001
    Serial.print("A001 ");
#endif ADDR_001
#ifdef ADDR_010
    expanderR3.begin(0x3a); // Address A-010
    Serial.print("A010 ");
#endif ADDR_010
#ifdef ADDR_011
    expanderR4.begin(0x3b); // Address A-011
    Serial.print("A011 ");
#endif ADDR_011
#ifdef ADDR_100
    expanderR5.begin(0x3c); // Address A-100
    Serial.print("A100 ");
#endif ADDR_100
#ifdef ADDR_101
    expanderR6.begin(0x3d); // Address A-101
    Serial.print("A101 ");
#endif ADDR_101
    Serial.println(" ");
  
    delay(2000);
  
    // Setup some PCF8574 pins for demo
    // Nota: default is 1 (note off)
#ifdef ADDR_A000
    expanderL1.pinMode(0, INPUT_PULLUP);
    expanderL1.pinMode(1, INPUT_PULLUP);
    expanderL1.pinMode(2, INPUT_PULLUP);
    expanderL1.pinMode(3, INPUT_PULLUP);
    expanderL1.pinMode(4, INPUT_PULLUP);
    expanderL1.pinMode(5, INPUT_PULLUP);
#endif ADDR_A000
  
#ifdef ADDR_A001
    expanderL2.pinMode(4, INPUT_PULLUP);
    expanderL2.pinMode(3, INPUT_PULLUP);
    expanderL2.pinMode(2, INPUT_PULLUP);
    expanderL2.pinMode(1, INPUT_PULLUP);
    expanderL2.pinMode(0, INPUT_PULLUP);
#endif ADDR_A001
    
#ifdef ADDR_A010
    expanderL3.pinMode(0, INPUT_PULLUP);
    expanderL3.pinMode(1, INPUT_PULLUP);
    expanderL3.pinMode(2, INPUT_PULLUP);
    expanderL3.pinMode(3, INPUT_PULLUP);
    expanderL3.pinMode(4, INPUT_PULLUP);
    expanderL3.pinMode(5, INPUT_PULLUP);
#endif ADDR_A010
  
#ifdef ADDR_A011
    expanderL4.pinMode(4, INPUT_PULLUP);
    expanderL4.pinMode(3, INPUT_PULLUP);
    expanderL4.pinMode(2, INPUT_PULLUP);
    expanderL4.pinMode(1, INPUT_PULLUP);
    expanderL4.pinMode(0, INPUT_PULLUP);
#endif ADDR_A011
  
#ifdef ADDR_A100
    expanderL5.pinMode(0, INPUT_PULLUP);
    expanderL5.pinMode(1, INPUT_PULLUP);
    expanderL5.pinMode(2, INPUT_PULLUP);
    expanderL5.pinMode(3, INPUT_PULLUP);
    expanderL5.pinMode(4, INPUT_PULLUP);
    expanderL5.pinMode(5, INPUT_PULLUP);
#endif ADDR_A100
  
#ifdef ADDR_A101
    expanderL6.pinMode(4, INPUT_PULLUP);
    expanderL6.pinMode(3, INPUT_PULLUP);
    expanderL6.pinMode(2, INPUT_PULLUP);
    expanderL6.pinMode(1, INPUT_PULLUP);
    expanderL6.pinMode(0, INPUT_PULLUP);
#endif ADDR_A101
  
#ifdef ADDR_000
    expanderR1.pinMode(0, INPUT_PULLUP);
    expanderR1.pinMode(1, INPUT_PULLUP);
    expanderR1.pinMode(2, INPUT_PULLUP);
    expanderR1.pinMode(3, INPUT_PULLUP);
    expanderR1.pinMode(4, INPUT_PULLUP);
    expanderR1.pinMode(5, INPUT_PULLUP);
#endif ADDR_000
  
#ifdef ADDR_001
    expanderR2.pinMode(0, INPUT_PULLUP);
    expanderR2.pinMode(1, INPUT_PULLUP);
    expanderR2.pinMode(2, INPUT_PULLUP);
    expanderR2.pinMode(3, INPUT_PULLUP);
    expanderR2.pinMode(4, INPUT_PULLUP);
    expanderR2.pinMode(5, INPUT_PULLUP);
    expanderR2.pinMode(6, INPUT_PULLUP);
#endif ADDR_001
  
#ifdef ADDR_010
//    expanderR3.pinMode(0, INPUT_PULLUP); // Not used
    expanderR3.pinMode(1, INPUT_PULLUP);
    expanderR3.pinMode(2, INPUT_PULLUP);
    expanderR3.pinMode(3, INPUT_PULLUP);
    expanderR3.pinMode(4, INPUT_PULLUP);
    expanderR3.pinMode(5, INPUT_PULLUP);
#endif ADDR_010
  
#ifdef ADDR_011
    expanderR4.pinMode(0, INPUT_PULLUP);
    expanderR4.pinMode(1, INPUT_PULLUP);
    expanderR4.pinMode(2, INPUT_PULLUP);
    expanderR4.pinMode(3, INPUT_PULLUP);
    expanderR4.pinMode(4, INPUT_PULLUP);
    expanderR4.pinMode(5, INPUT_PULLUP);
//    expanderR4.pinMode(6, INPUT_PULLUP); // Not used
#endif ADDR_011

#ifdef ADDR_100
    expanderR5.pinMode(0, INPUT_PULLUP);
    expanderR5.pinMode(1, INPUT_PULLUP);
    expanderR5.pinMode(2, INPUT_PULLUP);
    expanderR5.pinMode(3, INPUT_PULLUP);
    expanderR5.pinMode(4, INPUT_PULLUP);
    expanderR5.pinMode(5, INPUT_PULLUP);
#endif ADDR_100
  
#ifdef ADDR_101
    expanderR6.pinMode(0, INPUT_PULLUP);
    expanderR6.pinMode(1, INPUT_PULLUP);
    expanderR6.pinMode(2, INPUT_PULLUP);
    expanderR6.pinMode(3, INPUT_PULLUP);
    expanderR6.pinMode(4, INPUT_PULLUP);
    expanderR6.pinMode(5, INPUT_PULLUP);
//    expanderR6.pinMode(6, INPUT_PULLUP); // Not used
#endif ADDR_101

    // DigitalRead demo
    Serial.println("Yo !");

    // Diplay bit position on top
#ifdef DISPLAY_BIT
            Serial.print("L ");
            
#ifdef ADDR_A000
            Serial.print("1     ");
            Serial.print(" ");
#endif ADDR_A000
  
#ifdef ADDR_A001
            Serial.print("2    ");
            Serial.print(" ");
#endif ADDR_A001
          
#ifdef ADDR_A010
            Serial.print("3     ");
            Serial.print(" ");
#endif ADDR_A010
            
#ifdef ADDR_A011
            Serial.print("4    ");
            Serial.print(" ");
#endif ADDR_A011
            
#ifdef ADDR_A100
            Serial.print("5     ");
            Serial.print(" ");
#endif ADDR_A100
            
#ifdef ADDR_A101
            Serial.print("6    ");
            Serial.print(" ");
#endif ADDR_A101
            
//            Serial.print(" - ");

            Serial.println("");

            // Right Hand
            // Basic diplay (IT)
#ifdef ADDR_000
            Serial.print("1     ");
            Serial.print(" ");
#endif ADDR_000
            
#ifdef ADDR_001
            Serial.print("2      ");
            Serial.print(" ");
#endif ADDR_001
            
#ifdef ADDR_010
            Serial.print("3    ");
            Serial.print(" ");
#endif ADDR_010
            
#ifdef ADDR_011
            Serial.print("4    ");
            Serial.print(" ");
#endif ADDR_011
            
#ifdef ADDR_100
            Serial.print("5     ");
            Serial.print(" ");
#endif ADDR_100
            
#ifdef ADDR_101
            Serial.print("6     ");
            Serial.println(" R");
#endif ADDR_101

#endif DISPLAY_BIT
}

void loop()
{
    while(1)
    {
#ifdef USE_INT
        sleep_mode();
        
        if(newNote == true)
#endif
        {
            newNote = false;
            
            // Left Hand
#ifdef DISPLAY_BIT
            Serial.print("L ");
            
#ifdef ADDR_A000
            Serial.print(expanderL1.digitalRead(0) ? "_" : "o");
            Serial.print(expanderL1.digitalRead(1) ? "_" : "o");
            Serial.print(expanderL1.digitalRead(2) ? "_" : "o");
            Serial.print(expanderL1.digitalRead(3) ? "_" : "o");
            Serial.print(expanderL1.digitalRead(4) ? "_" : "o");
            Serial.print(expanderL1.digitalRead(5) ? "_" : "o");
            Serial.print(" ");
#endif ADDR_A000
  
#ifdef ADDR_A001
            Serial.print(expanderL2.digitalRead(0) ? "_" : "o");
            Serial.print(expanderL2.digitalRead(1) ? "_" : "o");
            Serial.print(expanderL2.digitalRead(2) ? "_" : "o");
            Serial.print(expanderL2.digitalRead(3) ? "_" : "o");
            Serial.print(expanderL2.digitalRead(4) ? "_" : "o");
            Serial.print(" ");
#endif ADDR_A001
          
#ifdef ADDR_A010
            Serial.print(expanderL3.digitalRead(0) ? "o" : "_"); // inverted
            Serial.print(expanderL3.digitalRead(1) ? "o" : "_"); // inverted
            Serial.print(expanderL3.digitalRead(2) ? "o" : "_"); // inverted
            Serial.print(expanderL3.digitalRead(3) ? "o" : "_"); // inverted
            Serial.print(expanderL3.digitalRead(4) ? "o" : "_"); // inverted
            Serial.print(expanderL3.digitalRead(5) ? "o" : "_"); // inverted
            Serial.print(" ");
#endif ADDR_A010
            
#ifdef ADDR_A011
            Serial.print(expanderL4.digitalRead(0) ? "o" : "_"); // inverted
            Serial.print(expanderL4.digitalRead(1) ? "o" : "_"); // inverted
            Serial.print(expanderL4.digitalRead(2) ? "o" : "_"); // inverted
            Serial.print(expanderL4.digitalRead(3) ? "o" : "_"); // inverted
            Serial.print(expanderL4.digitalRead(4) ? "o" : "_"); // inverted
            Serial.print(" ");
#endif ADDR_A011
            
#ifdef ADDR_A100
            Serial.print(expanderL5.digitalRead(0) ? "_" : "o");
            Serial.print(expanderL5.digitalRead(1) ? "_" : "o");
            Serial.print(expanderL5.digitalRead(2) ? "_" : "o");
            Serial.print(expanderL5.digitalRead(3) ? "_" : "o");
            Serial.print(expanderL5.digitalRead(4) ? "_" : "o");
            Serial.print(expanderL5.digitalRead(5) ? "_" : "o");
            Serial.print(" ");
#endif ADDR_A100
            
#ifdef ADDR_A101
            Serial.print(expanderL6.digitalRead(0) ? "_" : "o");
            Serial.print(expanderL6.digitalRead(1) ? "_" : "o");
            Serial.print(expanderL6.digitalRead(2) ? "_" : "o");
            Serial.print(expanderL6.digitalRead(3) ? "_" : "o");
            Serial.print(expanderL6.digitalRead(4) ? "_" : "o");
            Serial.print(" ");
#endif ADDR_A101
            
//            Serial.print(" - ");

            Serial.println("");

            // Right Hand
            // Basic diplay (IT)
#ifdef ADDR_000
            Serial.print(expanderR1.digitalRead(0) ? "o" : "_"); // Inverted
            Serial.print(expanderR1.digitalRead(1) ? "o" : "_"); // Inverted
            Serial.print(expanderR1.digitalRead(2) ? "o" : "_"); // Inverted
            Serial.print(expanderR1.digitalRead(3) ? "o" : "_"); // Inverted
            Serial.print(expanderR1.digitalRead(4) ? "o" : "_"); // Inverted
            Serial.print(expanderR1.digitalRead(5) ? "o" : "_"); // Inverted
            Serial.print(" ");
#endif ADDR_000
            
#ifdef ADDR_001
            Serial.print(expanderR2.digitalRead(0) ? "o" : "_"); // Inverted
            Serial.print(expanderR2.digitalRead(1) ? "o" : "_"); // Inverted
            Serial.print(expanderR2.digitalRead(2) ? "o" : "_"); // Inverted
            Serial.print(expanderR2.digitalRead(3) ? "o" : "_"); // Inverted
            Serial.print(expanderR2.digitalRead(4) ? "o" : "_"); // Inverted
            Serial.print(expanderR2.digitalRead(5) ? "o" : "_"); // Inverted
            Serial.print(expanderR2.digitalRead(6) ? "o" : "_"); // Inverted
            Serial.print(" ");
#endif ADDR_001
            
#ifdef ADDR_010
            //Serial.print(expanderR3.digitalRead(0) ? "_" : "o");
            Serial.print(expanderR3.digitalRead(1) ? "_" : "o");
            Serial.print(expanderR3.digitalRead(2) ? "_" : "o");
            Serial.print(expanderR3.digitalRead(3) ? "_" : "o");
            Serial.print(expanderR3.digitalRead(4) ? "_" : "o");
            Serial.print(expanderR3.digitalRead(5) ? "_" : "o");
            Serial.print(" ");
#endif ADDR_010
            
#ifdef ADDR_011
            Serial.print(expanderR4.digitalRead(0) ? "_" : "o");
            Serial.print(expanderR4.digitalRead(1) ? "_" : "o");
            Serial.print(expanderR4.digitalRead(2) ? "_" : "o");
            Serial.print(expanderR4.digitalRead(3) ? "_" : "o");
            Serial.print(expanderR4.digitalRead(4) ? "_" : "o");
            Serial.print(expanderR4.digitalRead(5) ? "_" : "o");
            //Serial.print(expanderR4.digitalRead(6) ? "_" : "o");
            Serial.print(" ");
#endif ADDR_011
            
#ifdef ADDR_100
            Serial.print(expanderR5.digitalRead(0) ? "_" : "o");
            Serial.print(expanderR5.digitalRead(1) ? "_" : "o");
            Serial.print(expanderR5.digitalRead(2) ? "_" : "o");
            Serial.print(expanderR5.digitalRead(3) ? "_" : "o");
            Serial.print(expanderR5.digitalRead(4) ? "_" : "o");
            Serial.print(expanderR5.digitalRead(5) ? "_" : "o");
            Serial.print(" ");
#endif ADDR_100
            
#ifdef ADDR_101
            Serial.print(expanderR6.digitalRead(0) ? "_" : "o");
            Serial.print(expanderR6.digitalRead(1) ? "_" : "o");
            Serial.print(expanderR6.digitalRead(2) ? "_" : "o");
            Serial.print(expanderR6.digitalRead(3) ? "_" : "o");
            Serial.print(expanderR6.digitalRead(4) ? "_" : "o");
            Serial.print(expanderR6.digitalRead(5) ? "_" : "o");
           //Serial.print(expanderR6.digitalRead(6) ? "_" : "o");
            Serial.println(" R");
#endif ADDR_101

#endif DISPLAY_BIT
          
            // Address display
#ifdef DISPLAY_ADDR
#ifdef ADDR_000
            Serial.print(expanderR1.digitalRead(0) ? "R1_0 " : ""); // Inverted
            Serial.print(expanderR1.digitalRead(1) ? "R1_1 " : ""); // Inverted
            Serial.print(expanderR1.digitalRead(2) ? "R1_2 " : ""); // Inverted
            Serial.print(expanderR1.digitalRead(3) ? "R1_3 " : ""); // Inverted
            Serial.print(expanderR1.digitalRead(4) ? "R1_4 " : ""); // Inverted
            Serial.print(expanderR1.digitalRead(5) ? "R1_5 " : ""); // Inverted
#endif ADDR_000
            
#ifdef ADDR_001
            Serial.print(expanderR2.digitalRead(0) ? "R2_0 " : ""); // Inverted
            Serial.print(expanderR2.digitalRead(1) ? "R2_1 " : ""); // Inverted
            Serial.print(expanderR2.digitalRead(2) ? "R2_2 " : ""); // Inverted
            Serial.print(expanderR2.digitalRead(3) ? "R2_3 " : ""); // Inverted
            Serial.print(expanderR2.digitalRead(4) ? "R2_4 " : ""); // Inverted
            Serial.print(expanderR2.digitalRead(5) ? "R2_5 " : ""); // Inverted
            Serial.print(expanderR2.digitalRead(6) ? "R2_6 " : ""); // Inverted
#endif ADDR_001
            
#ifdef ADDR_010
            //Serial.print(expanderR3.digitalRead(0) ? "_" : "o");
            Serial.print(expanderR3.digitalRead(1) ? "" : "R3_1 ");
            Serial.print(expanderR3.digitalRead(2) ? "" : "R3_2 ");
            Serial.print(expanderR3.digitalRead(3) ? "" : "R3_3 ");
            Serial.print(expanderR3.digitalRead(4) ? "" : "R3_4 ");
            Serial.print(expanderR3.digitalRead(5) ? "" : "R3_5 ");
#endif ADDR_010
            
#ifdef ADDR_011
            Serial.print(expanderR4.digitalRead(0) ? "" : "R4_0 ");
            Serial.print(expanderR4.digitalRead(1) ? "" : "R4_1 ");
            Serial.print(expanderR4.digitalRead(2) ? "" : "R4_2 ");
            Serial.print(expanderR4.digitalRead(3) ? "" : "R4_3 ");
            Serial.print(expanderR4.digitalRead(4) ? "" : "R4_4 ");
            Serial.print(expanderR4.digitalRead(5) ? "" : "R4_5 ");
            //Serial.print(expanderR4.digitalRead(6) ? "_" : "o");
#endif ADDR_011
            
#ifdef ADDR_100
            Serial.print(expanderR5.digitalRead(0) ? "" : "R5_0 ");
            Serial.print(expanderR5.digitalRead(1) ? "" : "R5_1 ");
            Serial.print(expanderR5.digitalRead(2) ? "" : "R5_2 ");
            Serial.print(expanderR5.digitalRead(3) ? "" : "R5_3 ");
            Serial.print(expanderR5.digitalRead(4) ? "" : "R5_4 ");
            Serial.print(expanderR5.digitalRead(5) ? "" : "R5_5 ");
#endif ADDR_100
            
#ifdef ADDR_101
            Serial.print(expanderR6.digitalRead(0) ? "" : "R6_0 ");
            Serial.print(expanderR6.digitalRead(1) ? "" : "R6_1 ");
            Serial.print(expanderR6.digitalRead(2) ? "" : "R6_2 ");
            Serial.print(expanderR6.digitalRead(3) ? "" : "R6_3 ");
            Serial.print(expanderR6.digitalRead(4) ? "" : "R6_4 ");
            Serial.print(expanderR6.digitalRead(5) ? "" : "R6_5 ");
            //Serial.print(expanderR6.digitalRead(6) ? "_" : "o");
#endif ADDR_101
            Serial.println("");

#ifdef ADDR_A000
            Serial.print(expanderL1.digitalRead(0) ? "" : "L1_0");
            Serial.print(expanderL1.digitalRead(1) ? "" : "L1_1");
            Serial.print(expanderL1.digitalRead(2) ? "" : "L1_2");
            Serial.print(expanderL1.digitalRead(3) ? "" : "L1_3");
            Serial.print(expanderL1.digitalRead(4) ? "" : "L1_4");
            Serial.print(expanderL1.digitalRead(5) ? "" : "L1_5");
            Serial.print(" ");
#endif ADDR_A000
  
#ifdef ADDR_A001
            Serial.print(expanderL2.digitalRead(0) ? "" : "L2_0");
            Serial.print(expanderL2.digitalRead(1) ? "" : "L2_1");
            Serial.print(expanderL2.digitalRead(2) ? "" : "L2_2");
            Serial.print(expanderL2.digitalRead(3) ? "" : "L2_3");
            Serial.print(expanderL2.digitalRead(4) ? "" : "L2_4");
            Serial.print(" ");
#endif ADDR_A001
          
#ifdef ADDR_A010
            Serial.print(expanderL3.digitalRead(0) ? "" : "L3_0");
            Serial.print(expanderL3.digitalRead(1) ? "" : "L3_1");
            Serial.print(expanderL3.digitalRead(2) ? "" : "L3_2");
            Serial.print(expanderL3.digitalRead(3) ? "" : "L3_3");
            Serial.print(expanderL3.digitalRead(4) ? "" : "L3_4");
            Serial.print(expanderL3.digitalRead(5) ? "" : "L3_5");
            Serial.print(" ");
#endif ADDR_A010
            
#ifdef ADDR_A011
            Serial.print(expanderL4.digitalRead(0) ? "" : "L4_0");
            Serial.print(expanderL4.digitalRead(1) ? "" : "L4_1");
            Serial.print(expanderL4.digitalRead(2) ? "" : "L4_2");
            Serial.print(expanderL4.digitalRead(3) ? "" : "L4_3");
            Serial.print(expanderL4.digitalRead(4) ? "" : "L4_4");
            Serial.print(" ");
#endif ADDR_A011
            
#ifdef ADDR_A100
            Serial.print(expanderL5.digitalRead(0) ? "" : "L5_0");
            Serial.print(expanderL5.digitalRead(1) ? "" : "L5_1");
            Serial.print(expanderL5.digitalRead(2) ? "" : "L5_2");
            Serial.print(expanderL5.digitalRead(3) ? "" : "L5_3");
            Serial.print(expanderL5.digitalRead(4) ? "" : "L5_4");
            Serial.print(expanderL5.digitalRead(5) ? "" : "L5_5");
            Serial.print(" ");
#endif ADDR_A100
            
#ifdef ADDR_A101
            Serial.print(expanderL6.digitalRead(0) ? "" : "L6_0");
            Serial.print(expanderL6.digitalRead(1) ? "" : "L6_1");
            Serial.print(expanderL6.digitalRead(2) ? "" : "L6_2");
            Serial.print(expanderL6.digitalRead(3) ? "" : "L6_3");
            Serial.print(expanderL6.digitalRead(4) ? "" : "L6_4");
            Serial.println("");
#endif ADDR_A101

#endif DISPLAY_ADDR

            // Avoid Arduino to get lost...
            delay(25);
        }
    }

//  Serial.println("Take it easy, Charlie...");
//  delay(500);
}
