
/*
 * Here is an adaptation of my 60's 
 * chromatic Fratelli-Crosio into
 * a Manoury chromatic keyboard. The
 * highest notes are transposed to
 * the lowest notes of the Argentinian
 * keyboard, down to C2 but C#2 is 
 * missing. Plan is to add an extra
 * button.
 * Also, those lowest 6 notes are not 
 * gathered by set of 3 notes but in a 
 * way that seems more practical for 
 * those far-away keys:
 * 
 *  (C#) F2  E  | Ab  G   F#
 *   Eb  D   C2 | B   Bb  A
 *                  F3   D
 *                   E    C#
 *                    Eb   C3
 * 
 *         H a n d     thumb
 *
 *  Showing F3 down to C2. C#2 is missing.
 */

// Notes
const int MIDI_NOTE_L1_0 = pitchG2b;
const int MIDI_NOTE_L1_1 = pitchB2b;
const int MIDI_NOTE_L1_2 = pitchG2;
const int MIDI_NOTE_L1_3 = pitchB2;
const int MIDI_NOTE_L1_4 = pitchA2b;
const int MIDI_NOTE_L1_5 = pitchC2;  //pitchA4;

const int MIDI_NOTE_L2_0 = pitchE2;  //pitchC5;
const int MIDI_NOTE_L2_1 = pitchD2;  //pitchB4b;
const int MIDI_NOTE_L2_2 = pitchF2;  //pitchD5b;
const int MIDI_NOTE_L2_3 = pitchE2b; //pitchB4;
const int MIDI_NOTE_L2_4 = pitchA4b;

const int MIDI_NOTE_L3_0 = pitchA4;
const int MIDI_NOTE_L3_1 = pitchG4b;
const int MIDI_NOTE_L3_2 = pitchE4;
const int MIDI_NOTE_L3_3 = pitchD4;
const int MIDI_NOTE_L3_4 = pitchC4;
const int MIDI_NOTE_L3_5 = pitchB3b;

const int MIDI_NOTE_L4_0 = pitchA3b;
const int MIDI_NOTE_L4_1 = pitchG3b;
const int MIDI_NOTE_L4_2 = pitchE3;
const int MIDI_NOTE_L4_3 = pitchD3;
const int MIDI_NOTE_L4_4 = pitchC3;

const int MIDI_NOTE_L5_0 = pitchA2;
const int MIDI_NOTE_L5_1 = pitchD3b;
const int MIDI_NOTE_L5_2 = pitchE3b;
const int MIDI_NOTE_L5_3 = pitchF3;
const int MIDI_NOTE_L5_4 = pitchG3;
const int MIDI_NOTE_L5_5 = pitchA3;

const int MIDI_NOTE_L6_0 = pitchB3;
const int MIDI_NOTE_L6_1 = pitchD4b;
const int MIDI_NOTE_L6_2 = pitchE4b;
const int MIDI_NOTE_L6_3 = pitchF4;
const int MIDI_NOTE_L6_4 = pitchG4;

