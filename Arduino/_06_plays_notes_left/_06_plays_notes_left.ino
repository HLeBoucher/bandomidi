
/*
 * Listing MIDI port:
 *  - aconnect -l
 * Monitoring events on a port:
 *  - aseqdump -p 24
 * Starting the synthetiser as a deamon (-K -2 for B flat):
 *  - timidity -iA -B2,8 -Os1l -s 44100 -K -2
 * Linking input with output
 *  - aconnect 24 128
 *  
 *  Nota: soundfont is selected in /etc/timidity/timidity.cfg
 */

/* Dependencies */
#include <Wire.h>     // I2C communication
#include <PCF8574.h>  // GPIO expander
//#include <PCint.h>
#include <MIDIUSB.h>
#include <pitchToNote.h>
#include <avr/sleep.h>

// Defines
//#define IT_MODE // IT_driven (otherwise: polling)

// Constants
const byte interruptPin = 0; // Rx Pin / INT2
const int MIDI_CHANNEL  = 1; // 0 is for right hand
const int MIDI_VELOCITY = 106; // Nota: 127 (max) sounds too agressive...

//#include "./notes_Crosio60_left.h" // Original
#include "./notes_Crosio60ManouryHaudo_left.h"


// PCF8574 instances
PCF8574 expanderL1; // bits 0-5
PCF8574 expanderL2; // bits 0-4
PCF8574 expanderL3; // bits 0-5 // inverted
PCF8574 expanderL4; // bits 0-4 // inverted
PCF8574 expanderL5; // bits 0-5
PCF8574 expanderL6; // bits 0-4

bool newNote;
byte L1_Note_Tminus1;
byte L1_Note_T;
byte L2_Note_Tminus1;
byte L2_Note_T;
byte L3_Note_Tminus1;
byte L3_Note_T;
byte L4_Note_Tminus1;
byte L4_Note_T;
byte L5_Note_Tminus1;
byte L5_Note_T;
byte L6_Note_Tminus1;
byte L6_Note_T;


/*******************************************************************************
 *                                                                             *
 *                               Interruption                                  *
 *                                                                             *
 *******************************************************************************/
#ifdef IT_MODE
void interruption()
{
//  Serial.print("!");
  newNote = true;
}
#endif // IT_MODE

/*******************************************************************************
 *                                                                             *
 *                             Initialization                                  *
 *                                                                             *
 *******************************************************************************/
void setup()
{
#ifdef IT_MODE
  pinMode(interruptPin, INPUT_PULLUP);
  newNote = false;
  attachInterrupt(digitalPinToInterrupt(interruptPin), interruption, FALLING);
  // Note that: "An interrupt is generated by any rising or 
  // falling edge of the port inputs". (PCF8574 datasheet)
#endif // IT_MODE

  // Setup serial for debug
//  Serial.begin(115200);

  delay(1000);

  // Extra delay required for re-programming the Micro
  // Avoid using the reset button
  delay(20000); // 20 seconds
  
  // Start I2C bus and PCF8574(A) instance
  expanderL1.begin(0x20); // Address 000
  expanderL2.begin(0x21); // Address 001
  expanderL3.begin(0x22); // Address 010
  expanderL4.begin(0x23); // Address 011
  expanderL5.begin(0x24); // Address 100
  expanderL6.begin(0x25); // Address 101
  
  // Setup some PCF8574 pins for demo
  // Nota: default is 1 (note off)
  expanderL1.pinMode(0, INPUT_PULLUP);
  expanderL1.pinMode(1, INPUT_PULLUP);
  expanderL1.pinMode(2, INPUT_PULLUP);
  expanderL1.pinMode(3, INPUT_PULLUP);
  expanderL1.pinMode(4, INPUT_PULLUP);
  expanderL1.pinMode(5, INPUT_PULLUP);
  
  expanderL2.pinMode(0, INPUT_PULLUP);
  expanderL2.pinMode(1, INPUT_PULLUP);
  expanderL2.pinMode(2, INPUT_PULLUP);
  expanderL2.pinMode(3, INPUT_PULLUP);
  expanderL2.pinMode(4, INPUT_PULLUP);
//  expanderL2.pinMode(5, INPUT_PULLUP);
//  expanderL2.pinMode(6, INPUT_PULLUP);

  expanderL3.pinMode(0, INPUT_PULLUP);
  expanderL3.pinMode(1, INPUT_PULLUP);
  expanderL3.pinMode(2, INPUT_PULLUP);
  expanderL3.pinMode(3, INPUT_PULLUP);
  expanderL3.pinMode(4, INPUT_PULLUP);
  expanderL3.pinMode(5, INPUT_PULLUP);
  
  expanderL4.pinMode(0, INPUT_PULLUP);
  expanderL4.pinMode(1, INPUT_PULLUP);
  expanderL4.pinMode(2, INPUT_PULLUP);
  expanderL4.pinMode(3, INPUT_PULLUP);
  expanderL4.pinMode(4, INPUT_PULLUP);
//  expanderL4.pinMode(5, INPUT_PULLUP);
//  expanderL4.pinMode(6, INPUT_PULLUP);

  expanderL5.pinMode(0, INPUT_PULLUP);
  expanderL5.pinMode(1, INPUT_PULLUP);
  expanderL5.pinMode(2, INPUT_PULLUP);
  expanderL5.pinMode(3, INPUT_PULLUP);
  expanderL5.pinMode(4, INPUT_PULLUP);
  expanderL5.pinMode(5, INPUT_PULLUP);
  
  expanderL6.pinMode(0, INPUT_PULLUP);
  expanderL6.pinMode(1, INPUT_PULLUP);
  expanderL6.pinMode(2, INPUT_PULLUP);
  expanderL6.pinMode(3, INPUT_PULLUP);
  expanderL6.pinMode(4, INPUT_PULLUP);
//  expanderL6.pinMode(5, INPUT_PULLUP);
//  expanderL6.pinMode(6, INPUT_PULLUP);

  // Initialize buffers
  L1_Note_Tminus1 =  expanderL1.read();
  L1_Note_T = L1_Note_Tminus1;
  L2_Note_Tminus1 =  expanderL2.read();
  L2_Note_T = L2_Note_Tminus1;
  L3_Note_Tminus1 = ~expanderL3.read(); // inverted
  L3_Note_T = L3_Note_Tminus1;
  L4_Note_Tminus1 = ~expanderL4.read(); // inverted
  L4_Note_T = L4_Note_Tminus1;
  L5_Note_Tminus1 =  expanderL5.read();
  L5_Note_T = L5_Note_Tminus1;
  L6_Note_Tminus1 =  expanderL6.read();
  L6_Note_T = L6_Note_Tminus1;

  // DigitalRead demo
//  Serial.println("Yo !");
}


/*******************************************************************************
 *                                                                             *
 *                                   Loop                                      *
 *                                                                             *
 *******************************************************************************/
void loop()
{
    while(1)
    {
#ifdef IT_MODE
        sleep_mode();
        // Check states of concerned expander
        // Update MIDI status of the note
        if(newNote == true)
        {
            newNote = false;
#endif // IT_MODE
      
            // Read concerned expanders
            L1_Note_T =  expanderL1.read();
            L2_Note_T =  expanderL2.read();
            L3_Note_T = ~expanderL3.read(); // inverted
            L4_Note_T = ~expanderL4.read(); // inverted
            L5_Note_T =  expanderL5.read();
            L6_Note_T =  expanderL6.read();
      
            // Compare and update
            UpdateL1();
            UpdateL2();
            UpdateL3();
            UpdateL4();
            UpdateL5();
            UpdateL6();
            
            // Flush!
            MidiUSB.flush();
#ifdef IT_MODE
       }
#endif // IT_MODE
 
        // Avoid loosing Arduino!
//        delay(1);
    }
}


/*******************************************************************************
 *                                                                             *
 *                             Extra-fonctions                                 *
 *                                                                             *
 *******************************************************************************/

            /*******************************************************
             *                      Midi                           *
             *******************************************************/

// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity)
{
    midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
    MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity)
{
    midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
    MidiUSB.sendMIDI(noteOff);
}

            /*******************************************************
             *                   Expander 1                        *
             *******************************************************/
void UpdateL1(void)
{
    bool bit_tmpA;
    bool bit_tmpB;
    
    if( L1_Note_T != L1_Note_Tminus1)
    {
/*Serial.print("reg: [");
Serial.print(L1_Note_T, BIN);
Serial.println("]");*/
        // Bit 0
        bit_tmpA = bitRead(L1_Note_T, 0);
        bit_tmpB = bitRead(L1_Note_Tminus1, 0);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L1_0, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L1_0, MIDI_VELOCITY);
        }
        
        // Bit 1
        bit_tmpA = bitRead(L1_Note_T, 1);
        bit_tmpB = bitRead(L1_Note_Tminus1, 1);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L1_1, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L1_1, MIDI_VELOCITY);
        }
        
        // Bit 2
        bit_tmpA = bitRead(L1_Note_T, 2);
        bit_tmpB = bitRead(L1_Note_Tminus1, 2);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L1_2, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L1_2, MIDI_VELOCITY);
        }
        
        // Bit 3
        bit_tmpA = bitRead(L1_Note_T, 3);
        bit_tmpB = bitRead(L1_Note_Tminus1, 3);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L1_3, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L1_3, MIDI_VELOCITY);
        }
        
        // Bit 4
        bit_tmpA = bitRead(L1_Note_T, 4);
        bit_tmpB = bitRead(L1_Note_Tminus1, 4);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L1_4, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L1_4, MIDI_VELOCITY);
        }
        
        // Bit 5
        bit_tmpA = bitRead(L1_Note_T, 5);
        bit_tmpB = bitRead(L1_Note_Tminus1, 5);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L1_5, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L1_5, MIDI_VELOCITY);
        }
        
        // Update buffer
        L1_Note_Tminus1 = L1_Note_T;
    }
}


            /*******************************************************
             *                   Expander 2                        *
             *******************************************************/
void UpdateL2(void)
{
    bool bit_tmpA;
    bool bit_tmpB;
    
    if( L2_Note_T != L2_Note_Tminus1)
    {
        // Bit 0
        bit_tmpA = bitRead(L2_Note_T, 0);
        bit_tmpB = bitRead(L2_Note_Tminus1, 0);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L2_0, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L2_0, MIDI_VELOCITY);
        }
        
        // Bit 1
        bit_tmpA = bitRead(L2_Note_T, 1);
        bit_tmpB = bitRead(L2_Note_Tminus1, 1);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L2_1, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L2_1, MIDI_VELOCITY);
        }
        
        // Bit 2
        bit_tmpA = bitRead(L2_Note_T, 2);
        bit_tmpB = bitRead(L2_Note_Tminus1, 2);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L2_2, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L2_2, MIDI_VELOCITY);
        }
        
        // Bit 3
        bit_tmpA = bitRead(L2_Note_T, 3);
        bit_tmpB = bitRead(L2_Note_Tminus1, 3);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L2_3, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L2_3, MIDI_VELOCITY);
        }
        
        // Bit 4
        bit_tmpA = bitRead(L2_Note_T, 4);
        bit_tmpB = bitRead(L2_Note_Tminus1, 4);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L2_4, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L2_4, MIDI_VELOCITY);
        }
        
/*        // Bit 5
        bit_tmpA = bitRead(L2_Note_T, 5);
        bit_tmpB = bitRead(L2_Note_Tminus1, 5);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L2_5, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L2_5, MIDI_VELOCITY);
        }
        
        // Bit 6
        bit_tmpA = bitRead(L2_Note_T, 6);
        bit_tmpB = bitRead(L2_Note_Tminus1, 6);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L2_6, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L2_6, MIDI_VELOCITY);
        }
*/        
        // Update buffer
        L2_Note_Tminus1 = L2_Note_T;
    }
}

            /*******************************************************
             *                   Expander 3                        *
             *******************************************************/
void UpdateL3(void)
{
    bool bit_tmpA;
    bool bit_tmpB;
    
    if( L3_Note_T != L3_Note_Tminus1)
    {
        // Bit 0
        bit_tmpA = bitRead(L3_Note_T, 0);
        bit_tmpB = bitRead(L3_Note_Tminus1, 0);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L3_0, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L3_0, MIDI_VELOCITY);
        }
        
        // Bit 1
        bit_tmpA = bitRead(L3_Note_T, 1);
        bit_tmpB = bitRead(L3_Note_Tminus1, 1);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L3_1, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L3_1, MIDI_VELOCITY);
        }
        
        // Bit 2
        bit_tmpA = bitRead(L3_Note_T, 2);
        bit_tmpB = bitRead(L3_Note_Tminus1, 2);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L3_2, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L3_2, MIDI_VELOCITY);
        }
        
        // Bit 3
        bit_tmpA = bitRead(L3_Note_T, 3);
        bit_tmpB = bitRead(L3_Note_Tminus1, 3);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L3_3, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L3_3, MIDI_VELOCITY);
        }
        
        // Bit 4
        bit_tmpA = bitRead(L3_Note_T, 4);
        bit_tmpB = bitRead(L3_Note_Tminus1, 4);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L3_4, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L3_4, MIDI_VELOCITY);
        }
        
        // Bit 5
        bit_tmpA = bitRead(L3_Note_T, 5);
        bit_tmpB = bitRead(L3_Note_Tminus1, 5);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L3_5, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L3_5, MIDI_VELOCITY);
        }

        // Update buffer
        L3_Note_Tminus1 = L3_Note_T;
    }
}

            /*******************************************************
             *                   Expander 4                        *
             *******************************************************/
void UpdateL4(void)
{
    bool bit_tmpA;
    bool bit_tmpB;
    
    if( L4_Note_T != L4_Note_Tminus1)
    {
        // Bit 0
        bit_tmpA = bitRead(L4_Note_T, 0);
        bit_tmpB = bitRead(L4_Note_Tminus1, 0);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L4_0, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L4_0, MIDI_VELOCITY);
        }
        
        // Bit 1
        bit_tmpA = bitRead(L4_Note_T, 1);
        bit_tmpB = bitRead(L4_Note_Tminus1, 1);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L4_1, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L4_1, MIDI_VELOCITY);
        }
        
        // Bit 2
        bit_tmpA = bitRead(L4_Note_T, 2);
        bit_tmpB = bitRead(L4_Note_Tminus1, 2);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L4_2, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L4_2, MIDI_VELOCITY);
        }
        
        // Bit 3
        bit_tmpA = bitRead(L4_Note_T, 3);
        bit_tmpB = bitRead(L4_Note_Tminus1, 3);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L4_3, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L4_3, MIDI_VELOCITY);
        }
        
        // Bit 4
        bit_tmpA = bitRead(L4_Note_T, 4);
        bit_tmpB = bitRead(L4_Note_Tminus1, 4);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L4_4, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L4_4, MIDI_VELOCITY);
        }
        
/*        // Bit 5
        bit_tmpA = bitRead(L4_Note_T, 5);
        bit_tmpB = bitRead(L4_Note_Tminus1, 5);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L4_5, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L4_5, MIDI_VELOCITY);
        }
*/
        // Update buffer
        L4_Note_Tminus1 = L4_Note_T;
    }
}

            /*******************************************************
             *                   Expander 5                        *
             *******************************************************/
void UpdateL5(void)
{
    bool bit_tmpA;
    bool bit_tmpB;
    
    if( L5_Note_T != L5_Note_Tminus1)
    {
        // Bit 0
        bit_tmpA = bitRead(L5_Note_T, 0);
        bit_tmpB = bitRead(L5_Note_Tminus1, 0);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L5_0, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L5_0, MIDI_VELOCITY);
        }
        
        // Bit 1
        bit_tmpA = bitRead(L5_Note_T, 1);
        bit_tmpB = bitRead(L5_Note_Tminus1, 1);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L5_1, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L5_1, MIDI_VELOCITY);
        }
        
        // Bit 2
        bit_tmpA = bitRead(L5_Note_T, 2);
        bit_tmpB = bitRead(L5_Note_Tminus1, 2);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L5_2, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L5_2, MIDI_VELOCITY);
        }
        
        // Bit 3
        bit_tmpA = bitRead(L5_Note_T, 3);
        bit_tmpB = bitRead(L5_Note_Tminus1, 3);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L5_3, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L5_3, MIDI_VELOCITY);
        }
        
        // Bit 4
        bit_tmpA = bitRead(L5_Note_T, 4);
        bit_tmpB = bitRead(L5_Note_Tminus1, 4);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L5_4, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L5_4, MIDI_VELOCITY);
        }
        
        // Bit 5
        bit_tmpA = bitRead(L5_Note_T, 5);
        bit_tmpB = bitRead(L5_Note_Tminus1, 5);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L5_5, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L5_5, MIDI_VELOCITY);
        }

        // Update buffer
        L5_Note_Tminus1 = L5_Note_T;
    }
}

            /*******************************************************
             *                   Expander 6                        *
             *******************************************************/
void UpdateL6(void)
{
    bool bit_tmpA;
    bool bit_tmpB;
    
    if( L6_Note_T != L6_Note_Tminus1)
    {
        // Bit 0
        bit_tmpA = bitRead(L6_Note_T, 0);
        bit_tmpB = bitRead(L6_Note_Tminus1, 0);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L6_0, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L6_0, MIDI_VELOCITY);
        }
        
        // Bit 1
        bit_tmpA = bitRead(L6_Note_T, 1);
        bit_tmpB = bitRead(L6_Note_Tminus1, 1);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L6_1, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L6_1, MIDI_VELOCITY);
        }
        
        // Bit 2
        bit_tmpA = bitRead(L6_Note_T, 2);
        bit_tmpB = bitRead(L6_Note_Tminus1, 2);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L6_2, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L6_2, MIDI_VELOCITY);
        }
        
        // Bit 3
        bit_tmpA = bitRead(L6_Note_T, 3);
        bit_tmpB = bitRead(L6_Note_Tminus1, 3);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L6_3, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L6_3, MIDI_VELOCITY);
        }
        
        // Bit 4
        bit_tmpA = bitRead(L6_Note_T, 4);
        bit_tmpB = bitRead(L6_Note_Tminus1, 4);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L6_4, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L6_4, MIDI_VELOCITY);
        }
        
/*        // Bit 5
        bit_tmpA = bitRead(L6_Note_T, 5);
        bit_tmpB = bitRead(L6_Note_Tminus1, 5);
        if(bit_tmpA != bit_tmpB)
        {
            if(bit_tmpA == 0)
                noteOn(MIDI_CHANNEL, MIDI_NOTE_L6_5, MIDI_VELOCITY);
            else
                noteOff(MIDI_CHANNEL, MIDI_NOTE_L6_5, MIDI_VELOCITY);
        }
*/
        // Update buffer
        L6_Note_Tminus1 = L6_Note_T;
    }
}
