
/*
 * Listing MIDI port:
 *  - aconnect -l
 * Monitoring events on a port:
 *  - aseqdump -p 24
 * Starting the synthetiser as a deamon (-K -2 for B flat):
 *  - timidity -iA -B2,8 -Os1l -s 44100 -K -2
 * Linking input with output
 *  - aconnect 24 128
 */

/* Dependencies */
#include <Wire.h>    // Required for I2C communication
#include <PCF8574.h>
//#include <PCint.h>
//#include <frequencyToNote.h>
//#include <pitchToNote.h>
#include <MIDIUSB.h>
//#include <pitchToFrequency.h>
#include <avr/sleep.h>

/** PCF8574 instance */
PCF8574 expanderR1;
PCF8574 expanderR2;
PCF8574 expanderL1;
PCF8574 expanderL2;

const byte interruptPin = 0; // Rx Pin / INT2

// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

void interruption()
{
  Serial.print("!");
  //expanderR2.read();
}

/** setup() */
void setup()
{
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), interruption, CHANGE);

  // Setup serial for debug
  Serial.begin(115200);
  
  // Start I2C bus and PCF8574 instance
/*  expanderL1.begin(0x20); // Address 000
  expanderL2.begin(0x21); // Address 001
  expanderR1.begin(0x38); // Address A-001
  expanderR2.begin(0x39); // Address A-010
*/  
/*  expanderL1.begin(0x22); // Address 011
  expanderL2.begin(0x23); // Address 100
  expanderR1.begin(0x3a); // Address A-011
  expanderR2.begin(0x3b); // Address A-100
*/  
/*  expanderL1.begin(0x24); // Address 100
  expanderL2.begin(0x25); // Address 101
  expanderR1.begin(0x3c); // Address A-100
*/  expanderR2.begin(0x3d); // Address A-101
  
  // Setup some PCF8574 pins for demo
  // Nota: default is 1 (note off)
/*  expanderL1.pinMode(0, INPUT_PULLUP);
  expanderL1.pinMode(1, INPUT_PULLUP);
  expanderL1.pinMode(2, INPUT_PULLUP);
  expanderL1.pinMode(3, INPUT_PULLUP);
  expanderL1.pinMode(4, INPUT_PULLUP);
  expanderL1.pinMode(5, INPUT_PULLUP);
  
  expanderL2.pinMode(4, INPUT_PULLUP);
  expanderL2.pinMode(3, INPUT_PULLUP);
  expanderL2.pinMode(2, INPUT_PULLUP);
  expanderL2.pinMode(1, INPUT_PULLUP);
  expanderL2.pinMode(0, INPUT_PULLUP);
  
  expanderR1.pinMode(0, INPUT_PULLUP);
  expanderR1.pinMode(1, INPUT_PULLUP);
  expanderR1.pinMode(2, INPUT_PULLUP);
  expanderR1.pinMode(3, INPUT_PULLUP);
  expanderR1.pinMode(4, INPUT_PULLUP);
  expanderR1.pinMode(5, INPUT_PULLUP);*/
  
  expanderR2.pinMode(0, INPUT_PULLUP);
  expanderR2.pinMode(1, INPUT_PULLUP);
  expanderR2.pinMode(2, INPUT_PULLUP);
  expanderR2.pinMode(3, INPUT_PULLUP);
  expanderR2.pinMode(4, INPUT_PULLUP);
  expanderR2.pinMode(5, INPUT_PULLUP);
  expanderR2.pinMode(6, INPUT_PULLUP);
  
  // DigitalRead demo
  Serial.println("Yo !");
}

/** loop() */
void loop()
{

  while(1)
  {
    sleep_mode();
    // Check states of concerned PCF
    // Update MIDI status of the note
  }
    
/*  Serial.println("Sending note on");
  noteOn(0, 48, 64);   // Channel 0, middle C, normal velocity
  MidiUSB.flush();
  delay(500);
  Serial.println("Sending note off");
  noteOff(0, 48, 64);  // Channel 0, middle C, normal velocity
  MidiUSB.flush();
  delay(1500);*/

/*  if(expanderL1.digitalRead(0))
    noteOff(0, 48, 64);
  else
    noteOn(0, 48, 64);
  
  if(expanderL1.digitalRead(1))
    noteOff(0, 50, 64);
  else
    noteOn(0, 50, 64);
  
  if(expanderL1.digitalRead(2))
    noteOff(0, 52, 64);
  else
    noteOn(0, 52, 64);
  
  MidiUSB.flush();
  delay(100);*/
 

//  Serial.print(expanderR1.read(), BIN); // Read the whole pins input register

  // Left Hand
  Serial.print("L ");
  Serial.print(expanderL1.digitalRead(0) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(1) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(2) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(3) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(4) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(5) ? "_" : "o");
  
  Serial.print(expanderL2.digitalRead(0) ? "_" : "o");
  Serial.print(expanderL2.digitalRead(1) ? "_" : "o");
  Serial.print(expanderL2.digitalRead(2) ? "_" : "o");
  Serial.print(expanderL2.digitalRead(3) ? "_" : "o");
  Serial.print(expanderL2.digitalRead(4) ? "_" : "o");

  Serial.print(" - ");

  // Right Hand
  Serial.print(expanderR1.digitalRead(0) ? "_" : "o");
  Serial.print(expanderR1.digitalRead(1) ? "_" : "o");
  Serial.print(expanderR1.digitalRead(2) ? "_" : "o");
  Serial.print(expanderR1.digitalRead(3) ? "_" : "o");
  Serial.print(expanderR1.digitalRead(4) ? "_" : "o");
  Serial.print(expanderR1.digitalRead(5) ? "_" : "o");
  
  Serial.print(expanderR2.digitalRead(0) ? "_" : "o");
  Serial.print(expanderR2.digitalRead(1) ? "_" : "o");
  Serial.print(expanderR2.digitalRead(2) ? "_" : "o");
  Serial.print(expanderR2.digitalRead(3) ? "_" : "o");
  Serial.print(expanderR2.digitalRead(4) ? "_" : "o");
  Serial.print(expanderR2.digitalRead(5) ? "_" : "o");
  Serial.print(expanderR2.digitalRead(6) ? "_" : "o");
  Serial.println(" R");
  delay(100);

/*  Serial.println("Take it easy, Charlie...");
  delay(1000);*/
}

