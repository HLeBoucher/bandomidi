
/*
 * Listing MIDI port:
 *  - aconnect -l
 * Monitoring events on a port:
 *  - aseqdump -p 24
 * Starting the synthetiser as a deamon (-K -2 for B flat):
 *  - timidity -iA -B2,8 -Os1l -s 44100 -K -2
 * Linking input with output
 *  - aconnect 24 128
 */

/* Dependencies */
#include <Wire.h>    // Required for I2C communication
#include <PCF8574.h>
//#include <PCint.h>
//#include <frequencyToNote.h>
//#include <pitchToNote.h>
#include <MIDIUSB.h>
//#include <pitchToFrequency.h>
#include <avr/sleep.h>

// Constants
const byte interruptPin = 0; // Rx Pin / INT2

// PCF8574 instances
PCF8574 expanderR1; // inverted
PCF8574 expanderR2; // inverted
PCF8574 expanderR3;
PCF8574 expanderR4;
PCF8574 expanderR5;
PCF8574 expanderR6;
PCF8574 expanderL1;
PCF8574 expanderL2;
PCF8574 expanderL3;
PCF8574 expanderL4;
PCF8574 expanderL5;
PCF8574 expanderL6;
bool newNote;
//byte R2_Note_Tminus1;
//byte R2_Note_T;

// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

void interruption()
{
//  Serial.print("!");
  newNote = true;
}

void setup()
{
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), interruption, CHANGE);
  newNote = false;

  // Setup serial for debug
  Serial.begin(115200);

  delay(1000);
  Serial.println("In0");
  delay(1000);
  Serial.println("In1");
  
  // Start I2C bus and PCF8574(A) instance
//  expanderL1.begin(0x20); // Address 000
//  Serial.print("000 ");
//  expanderL2.begin(0x21); // Address 001
//  Serial.print("001 ");
//  expanderL3.begin(0x22); // Address 010
//  Serial.print("010 ");
//  expanderL4.begin(0x23); // Address 011
//  Serial.print("011 ");
//  expanderL5.begin(0x24); // Address 100
//  Serial.print("100 ");
//  expanderL6.begin(0x25); // Address 101
//  Serial.print("101 ");

  expanderR1.begin(0x38); // Address A-000
  Serial.print("A000 ");
  expanderR2.begin(0x39); // Address A-001
  Serial.print("A001 ");
  expanderR3.begin(0x3a); // Address A-010
  Serial.print("A010 ");
  expanderR4.begin(0x3b); // Address A-011
  Serial.print("A011 ");
  expanderR5.begin(0x3c); // Address A-100
  Serial.print("A100 ");
  expanderR6.begin(0x3d); // Address A-101
  Serial.print("A101 ");
  Serial.println(" ");
  delay(2000);
  
  // Setup some PCF8574 pins for demo
  // Nota: default is 1 (note off)
//  expanderL1.pinMode(0, INPUT_PULLUP);
//  expanderL1.pinMode(1, INPUT_PULLUP);
//  expanderL1.pinMode(2, INPUT_PULLUP);
//  expanderL1.pinMode(3, INPUT_PULLUP);
//  expanderL1.pinMode(4, INPUT_PULLUP);
//  expanderL1.pinMode(5, INPUT_PULLUP);
  
//  expanderL2.pinMode(4, INPUT_PULLUP);
//  expanderL2.pinMode(3, INPUT_PULLUP);
//  expanderL2.pinMode(2, INPUT_PULLUP);
//  expanderL2.pinMode(1, INPUT_PULLUP);
//  expanderL2.pinMode(0, INPUT_PULLUP);
  
//  expanderL3.pinMode(0, INPUT_PULLUP);
//  expanderL3.pinMode(1, INPUT_PULLUP);
//  expanderL3.pinMode(2, INPUT_PULLUP);
//  expanderL3.pinMode(3, INPUT_PULLUP);
//  expanderL3.pinMode(4, INPUT_PULLUP);
//  expanderL3.pinMode(5, INPUT_PULLUP);
  
//  expanderL4.pinMode(4, INPUT_PULLUP);
//  expanderL4.pinMode(3, INPUT_PULLUP);
//  expanderL4.pinMode(2, INPUT_PULLUP);
//  expanderL4.pinMode(1, INPUT_PULLUP);
//  expanderL4.pinMode(0, INPUT_PULLUP);
  
//  expanderL5.pinMode(0, INPUT_PULLUP);
//  expanderL5.pinMode(1, INPUT_PULLUP);
//  expanderL5.pinMode(2, INPUT_PULLUP);
//  expanderL5.pinMode(3, INPUT_PULLUP);
//  expanderL5.pinMode(4, INPUT_PULLUP);
//  expanderL5.pinMode(5, INPUT_PULLUP);
  
//  expanderL6.pinMode(4, INPUT_PULLUP);
//  expanderL6.pinMode(3, INPUT_PULLUP);
//  expanderL6.pinMode(2, INPUT_PULLUP);
//  expanderL6.pinMode(1, INPUT_PULLUP);
//  expanderL6.pinMode(0, INPUT_PULLUP);
  
  expanderR1.pinMode(0, INPUT_PULLUP);
  expanderR1.pinMode(1, INPUT_PULLUP);
  expanderR1.pinMode(2, INPUT_PULLUP);
  expanderR1.pinMode(3, INPUT_PULLUP);
  expanderR1.pinMode(4, INPUT_PULLUP);
  expanderR1.pinMode(5, INPUT_PULLUP);
  
  expanderR2.pinMode(0, INPUT_PULLUP);
  expanderR2.pinMode(1, INPUT_PULLUP);
  expanderR2.pinMode(2, INPUT_PULLUP);
  expanderR2.pinMode(3, INPUT_PULLUP);
  expanderR2.pinMode(4, INPUT_PULLUP);
  expanderR2.pinMode(5, INPUT_PULLUP);
  expanderR2.pinMode(6, INPUT_PULLUP);
//  R2_Note_Tminus1 = expanderR2.read();
//  R2_Note_T       = R2_Note_Tminus1;

  expanderR3.pinMode(0, INPUT_PULLUP);
  expanderR3.pinMode(1, INPUT_PULLUP);
  expanderR3.pinMode(2, INPUT_PULLUP);
  expanderR3.pinMode(3, INPUT_PULLUP);
  expanderR3.pinMode(4, INPUT_PULLUP);
  expanderR3.pinMode(5, INPUT_PULLUP);
  
  expanderR4.pinMode(0, INPUT_PULLUP);
  expanderR4.pinMode(1, INPUT_PULLUP);
  expanderR4.pinMode(2, INPUT_PULLUP);
  expanderR4.pinMode(3, INPUT_PULLUP);
  expanderR4.pinMode(4, INPUT_PULLUP);
  expanderR4.pinMode(5, INPUT_PULLUP);
  expanderR4.pinMode(6, INPUT_PULLUP);

  expanderR5.pinMode(0, INPUT_PULLUP);
  expanderR5.pinMode(1, INPUT_PULLUP);
  expanderR5.pinMode(2, INPUT_PULLUP);
  expanderR5.pinMode(3, INPUT_PULLUP);
  expanderR5.pinMode(4, INPUT_PULLUP);
  expanderR5.pinMode(5, INPUT_PULLUP);
  
  expanderR6.pinMode(0, INPUT_PULLUP);
  expanderR6.pinMode(1, INPUT_PULLUP);
  expanderR6.pinMode(2, INPUT_PULLUP);
  expanderR6.pinMode(3, INPUT_PULLUP);
  expanderR6.pinMode(4, INPUT_PULLUP);
  expanderR6.pinMode(5, INPUT_PULLUP);
  expanderR6.pinMode(6, INPUT_PULLUP);

  // DigitalRead demo
  Serial.println("Yo !");
}

void loop()
{

  while(1)
  {
    sleep_mode();
    // Check states of concerned PCF
    // Update MIDI status of the note
    if(newNote == true)
    {
      newNote = false;
//      Serial.println(expanderR2.read(), BIN);
      /*
//      R2_Note_T = expanderR2.read();
      if(expanderR2.digitalRead(0) == 0)
        Serial.println("on");
//        noteOn(0, 48, 64);
      else
        Serial.println("off");
//        noteOff(0, 48, 64);
//      MidiUSB.flush();
      */
//    }
/*    if(R2_Note_T != R2_Note_Tminus1)
    {
      R2_Note_Tminus1 = R2_Note_T;
      Serial.print("got: ");
      Serial.println(R2_Note_Tminus1, BIN);
    }*/
//  }
    
/*  Serial.println("Sending note on");
  noteOn(0, 48, 64);   // Channel 0, middle C, normal velocity
  MidiUSB.flush();
  delay(500);
  Serial.println("Sending note off");
  noteOff(0, 48, 64);  // Channel 0, middle C, normal velocity
  MidiUSB.flush();
  delay(1500);*/

/*  if(expanderR2.digitalRead(0))
    noteOff(0, 48, 64);
  else
    noteOn(0, 48, 64);
  
  if(expanderR2.digitalRead(1))
    noteOff(0, 50, 64);
  else
    noteOn(0, 50, 64);
  
  if(expanderR2.digitalRead(2))
    noteOff(0, 52, 64);
  else
    noteOn(0, 52, 64);
  
  MidiUSB.flush();
  delay(100);*/
 

//  Serial.print(expanderR1.read(), BIN); // Read the whole pins input register

  // Left Hand
/*  Serial.print("L ");
  Serial.print(expanderL1.digitalRead(0) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(1) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(2) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(3) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(4) ? "_" : "o");
  Serial.print(expanderL1.digitalRead(5) ? "_" : "o");
  Serial.print(" ");
  
  Serial.print(expanderL2.digitalRead(0) ? "_" : "o");
  Serial.print(expanderL2.digitalRead(1) ? "_" : "o");
  Serial.print(expanderL2.digitalRead(2) ? "_" : "o");
  Serial.print(expanderL2.digitalRead(3) ? "_" : "o");
  Serial.print(expanderL2.digitalRead(4) ? "_" : "o");
  Serial.print(" ");

  Serial.print(" - ");
*/
  // Right Hand
  Serial.print(expanderR1.digitalRead(0) ? "o" : "_"); // Inverted
  Serial.print(expanderR1.digitalRead(1) ? "o" : "_");
  Serial.print(expanderR1.digitalRead(2) ? "o" : "_");
  Serial.print(expanderR1.digitalRead(3) ? "o" : "_");
  Serial.print(expanderR1.digitalRead(4) ? "o" : "_");
  Serial.print(expanderR1.digitalRead(5) ? "o" : "_");
  Serial.print(" ");
  
  Serial.print(expanderR2.digitalRead(0) ? "o" : "_"); // Inverted
  Serial.print(expanderR2.digitalRead(1) ? "o" : "_");
  Serial.print(expanderR2.digitalRead(2) ? "o" : "_");
  Serial.print(expanderR2.digitalRead(3) ? "o" : "_");
  Serial.print(expanderR2.digitalRead(4) ? "o" : "_");
  Serial.print(expanderR2.digitalRead(5) ? "o" : "_");
  Serial.print(expanderR2.digitalRead(6) ? "o" : "_");
  Serial.print(" ");
  
  Serial.print(expanderR3.digitalRead(0) ? "_" : "o");
  Serial.print(expanderR3.digitalRead(1) ? "_" : "o");
  Serial.print(expanderR3.digitalRead(2) ? "_" : "o");
  Serial.print(expanderR3.digitalRead(3) ? "_" : "o");
  Serial.print(expanderR3.digitalRead(4) ? "_" : "o");
  Serial.print(expanderR3.digitalRead(5) ? "_" : "o");
  Serial.print(" ");
  
  Serial.print(expanderR4.digitalRead(0) ? "_" : "o");
  Serial.print(expanderR4.digitalRead(1) ? "_" : "o");
  Serial.print(expanderR4.digitalRead(2) ? "_" : "o");
  Serial.print(expanderR4.digitalRead(3) ? "_" : "o");
  Serial.print(expanderR4.digitalRead(4) ? "_" : "o");
  Serial.print(expanderR4.digitalRead(5) ? "_" : "o");
  Serial.print(expanderR4.digitalRead(6) ? "_" : "o");
  Serial.print(" ");
  
  Serial.print(expanderR5.digitalRead(0) ? "_" : "o");
  Serial.print(expanderR5.digitalRead(1) ? "_" : "o");
  Serial.print(expanderR5.digitalRead(2) ? "_" : "o");
  Serial.print(expanderR5.digitalRead(3) ? "_" : "o");
  Serial.print(expanderR5.digitalRead(4) ? "_" : "o");
  Serial.print(expanderR5.digitalRead(5) ? "_" : "o");
  Serial.print(" ");
  
  Serial.print(expanderR6.digitalRead(0) ? "_" : "o");
  Serial.print(expanderR6.digitalRead(1) ? "_" : "o");
  Serial.print(expanderR6.digitalRead(2) ? "_" : "o");
  Serial.print(expanderR6.digitalRead(3) ? "_" : "o");
  Serial.print(expanderR6.digitalRead(4) ? "_" : "o");
  Serial.print(expanderR6.digitalRead(5) ? "_" : "o");
  Serial.print(expanderR6.digitalRead(6) ? "_" : "o");
  Serial.println(" R");
  delay(100);
    }
  }

/*  Serial.println("Take it easy, Charlie...");
  delay(1000);*/
}

