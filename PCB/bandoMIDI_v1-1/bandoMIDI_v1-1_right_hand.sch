EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:bandoMIDI_v1-1_lib
LIBS:bandoMIDI_v1-1_right_hand-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "BandoMIDI"
Date "2018-03-04"
Rev "A1"
Comp "HLB"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1700 3050 1200 450 
U 5A9D59B2
F0 "RA" 60
F1 "bandoMIDI_RA.sch" 60
$EndSheet
Text Notes 1150 1300 0    118  ~ 0
Right-hand keyboard contains 3 rows of 11 to 13 keys:
Text HLabel 2900 3300 0    60   3State ~ 0
+5VL
Text HLabel 2900 3400 0    60   BiDi ~ 0
Gnd
Text HLabel 2900 3150 0    60   Input ~ 0
/INT
$Sheet
S 3450 3050 1200 450 
U 5A9D6D39
F0 "RB" 60
F1 "bandoMIDI_RB.sch" 60
$EndSheet
Text HLabel 3450 3150 2    60   Input ~ 0
/INT
Text HLabel 3450 3300 2    60   BiDi ~ 0
+5VL
Text HLabel 3450 3400 2    60   BiDi ~ 0
Gnd
Text Notes 1150 5100 0    98   ~ 0
RA 1 - 2 - 3 - 4 - 5 - 6 - RB 1 - 2 - 3 - 4 - 5 - 6\nRA 1 - 2 - 3 - 4 - 5 - 6 - RB 1 - 2 - 3 - 4 - 5 - 6 - 7\nRA 1 - 2 - 3 - 4 - 5 - 6 - RB 1 - 2 - 3 - 4 - 5
Text Notes 4050 4000 0    157  ~ 0
x3
Wire Wire Line
	2900 3150 3450 3150
Wire Wire Line
	3450 3300 2900 3300
Wire Wire Line
	2900 3400 3450 3400
Text Notes 1850 2600 0    118  ~ 0
-----------------\nRA(0x20) 6  - RB(0x21) 6\nRA(0x22) 6  - RB(0x23) 7\nRA(0x24) 6  - RB(0x25) 5\n           HAND\n-----------------
Text Notes 1150 1500 0    79   ~ 0
So the design will actually be three times the same 13 keys board.
$EndSCHEMATC
