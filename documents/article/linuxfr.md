# Article pour Linuxfr

## Électronique sous GNU/Linux - 15 ans de libre plus tard

Cher journal,

Ça fait plus de 15 ans que mon ordi tourne sous GNU/Linux, que je profite de tes articles et puis là je me suis dit bon sang que de chemin parcouru par le libre & les libristes. Je venais tout juste de terminer un projet complet, balayant toutes les étapes de conception à l'utilisation, alors ça y est, je me suis dit qu'il fallait raconter, partager...

Premier petit aparté pour signaler que je suis électronicien de métier (matériel/logiciel embarqué), que j'utilise les mêmes logiciels au boulot et que cet article serait le même si je présentais un projet de ma boite mais mon patron serait moins partant pour un tel déballage !

<div align="center"><img src="./img/05_connected_Fuyes.jpg" alt="Connected Fuyes" width="500"/></div>

<div align="center">La première version fonctionnelle.</div>

## L'histoire
Il y a un moment déjà, je me suis mis au bandonéon, qui est l'accordéon du tango. C'est un instrument qui peut être très sonore : pendant quelques années j'ai possédé une cabine acoustique (une sorte de cabine téléphonique pour faire des prises de voix par exemple) et puis j'ai déménagé, je l'ai revendue et je me suis de nouveau senti coincé par la vie en appartement. Il fallait que je trouve une solution pour m'exercer chez moi sans gêner les voisins.
Il existe des systèmes MIDI qui s'adaptent sur des accordéons mais je n'ai rien trouvé de spécifique au bandonéon. Je me suis donc lancé dans ce projet pour continuer à jouer discrètement l'hiver chez moi (l'été je m'installe au soleil).

## La réalisation
Il y a plusieurs façons de détecter le déclenchement d'une note : la plus chic, celle qui ne modifie pas l'instrument, est l'installation d'aimants sur les touches et de capteurs magnétiques en face. Le bandonéon est un instrument très compact et la conception de circuits imprimés ad-hoc a été nécessaire. J'ai fait un ensemble de cartes, main droite, main gauche, intégrant les capteurs à effet Hall, de quoi les alimenter et un composant "tampon" qui stocke l'état de chaque capteur, c'est-à-dire de chaque touche. À cela j'ai ajouté une carte Arduino Micro qui lit l'état des boutons et génère les évènements MIDI correspondants.

<<<<<<< HEAD
<div align="center"><img src="./img/11b_schematic_KiCAD_zoom.png" alt="Schematic with KiCAD" width="400"/></div>
<div align="center"><img src="./img/11c_PCB_KiCAD_zoom.png" alt="PCB with KiCAD" width="400"/></div>

<div align="center">Les schémas sont en blanc (Eeschema) et les PCB en noir (Pcbnew). Sinon ça ne pourrait pas fonctionner…</div></br>

<div align="center"><img src="./img/bandoMIDI_v1-1_left_hand.jpg" alt="PCB with KiCAD" width="400"/></div>

<div align="center">Vue 3d depuis KiCAD</div></br>
=======
<div align="center"><img src="./img/11b_schematic_KiCAD_zoom.png" alt="Schematic with KiCAD" width="400"/><img src="./img/11c_PCB_KiCAD_zoom.png" alt="PCB with KiCAD" width="400"/></div>

<div align="center">Les schémas sont en blanc (Eeschema) et les PCB en noir (Pcbnew). Sinon ça ne pourrait pas fonctionner…</div>


<div align="center"><img src="./img/bandoMIDI_v1-1_left_hand.jpg" alt="PCB with KiCAD" width="400"/></div>

<div align="center">Vue 3d depuis KiCAD</div>
>>>>>>> f39547d1776e90bd4eb1539dee320befc4b8d5d1

Le bandonéon est l'un des rares instruments stéréo : les sonorités de la main droite sont différentes de celles de la main gauche car ce côté-ci possède une petite caisse de résonance. Pour cette raison et aussi pour éviter de passer du câble à l'intérieur soufflet, j'ai donc installé un module Arduino de chaque côté, générant donc 2 instruments MIDI.

<div align="center"><img src="./img/02_PCB_installed.jpg" alt="Magnet sensor" width="300"/></div>

<<<<<<< HEAD
<div align="center">Les aimants sont installés à l'aide de Patafix.</div></br>
=======
<div align="center">Les aimants sont installés à l'aide de Patafix.</div>
>>>>>>> f39547d1776e90bd4eb1539dee320befc4b8d5d1

Côté ordinateur, les modules Arduino sont détectés comme des instruments MIDI, fluidsynth (sans JACK) s'occupe de faire le lien entre ces *devices* MIDI et une banque de sons. On a souvent dit (moi aussi...) du mal de la gestion du son sous GNU/Linux et bien j'ai été bluffé de voir que la mise en œuvre a été très simple et que mes modules Arduino ont causé du premier coup ! J'ai simplement câblé mes périphériques MIDI sur Musescore, un éditeur de partition, avec lequel je fais des transcriptions (repique d'un morceau à l'oreille). Là encore, chapeau : utilisation simple, logiciel stable, résultat nickel, bravo le libre !
Pour jouer simplement, de manière autonome, je me suis fait un synthétiseur portable dédié : je fais tourner fluidsynth sur un Odroid C1+ avec une cape HiFi qui sort en connecteurs RCA direction l'ampli. Et ça fonctionne.

Je ne sais pas qui peut être intéressé par ce projet, qu'importe, j'ai tout mis en ligne sur Gitlab.

## Au taff
Les différences avec le boulot (petite structure de ~25 p dont 8 dev) :
 - pour l'OS, aucune : j'utilise la dernière Ubuntu.
 - pour le schéma/le routage, aucune : j'utilise KiCAD, pour des circuits qui font jusqu'à 4 couches avec des microcontrôleurs et un peu de HF. Que de progrès depuis la première version que j'ai utilisée où la fonction "annuler" n'existait pas (sic). C'est complet, efficace, la gestion de schémas/routages sur d'anciennes versions réserve malheureusement quelques surprises.
 - pour la mécanique, aucune : j'utilise aussi FreeCAD, essentiellement pour de la visualisation mais aussi pour concevoir de petites pièces de test, généralement de l'impression 3d.
 - sur Arduino : le code est un peu sale pour moi qui ai l'habitude du C embarqué, avec des bibliothèques bien rangées, un programmateur/débogueur fiable... Mais pour ce genre de choses simples, c'est tout à fait suffisant. Il m'arrive d'ailleurs d'utiliser des modules Arduino au boulot dans le cadre de tests : rapides à mettre en œuvre, si je veux tester un composant, un capteur, c'est vite câblé et j'ai rapidement des réponses à mes questions.
 - pour le soft embarqué j'utilise les outils de ST Micro ou de Silabs, essentiellement TrueStudio qui est un Eclipse adapté aux microcontrôleurs ST. Je couple ça à une sonde Segger. C'est lourd, pas libre, mais ça tourne en natif sur mon poste Ubuntu. Même chose pour mon multimètre numérique de chez OTII : il y a un paquet debian. Ça n'est pas libre, mais le simple fait que ça tourne sous GNU/Linux c'est vraiment un changement incroyable par rapport à tous les outils (Microchip, WinAVR...) qui pompaient GCC pour ne le faire tourner que sur du proprio.

## D'autres projets
Le projet s'est un peu étalé (comme au boulot finalement !) et entre temps d'autres idées, par ailleurs libres, ont été publiées :
 - un bandonéon complètement MIDI en impression 3D par des étudiants uruguayens : [Bandonberry sur Github](https://github.com/jebentancour/Bandonberry "Sur github").
 - un concertina anglais : [Sur hackaday](https://hackaday.com/2019/08/18/midi-controller-in-a-concertina-looks-sea-shanty-ready/ "Sur hackaday").

## La suite
Aucune : pour le moment, je joue ! Bien sûr c'est améliorable, et comme c'est libre, avec un peu de chance, ce sera fait par d'autres.

<div align="center"><img src="./img/09_left_hand_playing.jpg" alt="Left hand playing" width="200"/></div>

<<<<<<< HEAD
<div align="center">De l'USB sur une mécanique des années 60.</div></br>
=======
<div align="center">De l'USB sur une mécanique des années 60.</div>
>>>>>>> f39547d1776e90bd4eb1539dee320befc4b8d5d1


## Des liens :
 - Mon dépot [Gitlab](https://gitlab.com/HLeBoucher/bandomidi "Dépot Gitlab") (j'y ai hébergé 2 vidéos de démonstration, je ne sais pas les vidéos sur Gitlab sont ce qu'il y a de plus judicieux à faire)…
 - Le projet sur [Hackaday](https://hackaday.io/project/170753-bandomidi "Hackaday").
 - Il existe une [banque de son](http://joergbleymehl.de/en/bandochords/soundfont/ "soundfont") de bandonéon qui a été réalisée par un passionné avec son propre instrument (pas de licence).
 - [Musescore](https://musescore.com/user/21006511 "Musescore"), un joli logiciel et une bibliothèque de partitions qui s'étoffe.
 - Une [interview [en]](http://blog.snapeda.com/2019/10/28/an-interview-with-wayne-stambaugh-of-kicad/ "Sur SnapEDAairuseauin") du lead dev de KiCAD.

