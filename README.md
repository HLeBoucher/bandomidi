# BandoMIDI

Hardware and software that turns an existing bandoneon into a MIDI instrument.

<div align="center"><img src="./documents/article/img/05_connected_Fuyes.jpg" alt="Connected Fuyes" width="500"/></div>

## In simple words
For years I owned a soundproofed cabin where I played the bandoneon without bothering anybody. I sold this cabin while moving to another flat and faced again the issue of practicing silently. As my day-to-day job is embedded systems, I slowly thought about turning my instrument into a MIDI one. That's how that project came to life.

Please find here everything required to make you own (and feel free to contact me)!

### What it does.
 - when you press a key, it generates a MIDI note.
 - there is one instrument by hand. This means you can have different soundfonts on right and left hands.

### What it does not.
 - being silent means no bellow. Since there is no register (as in an accordeon or an organ), using the bellow will sound. Big limitation, I know.
 - as I play chromatic bandoneon (similar notes either pushing or pulling), I did not take into account specificities related to bi-sonoric bandoneon.
 - it does not manage intensity: notes are On or Off. Though it would not be hard to add a basic pressure sensor.
 - the 4 highest notes of the right hand are not MIDIfied: mechanically speaking, they are completely apart and since I never play them (do you?) I decided that they do not exist.
 - it is not wireless. But it would be a great idea: finally cables are a bit annoying.

<div align="center">![Inside](./documents/videos/01_inside.webm)</div>

<div align="center">See the mechanism from the inside. Note the Arduino in the back.</div>

<div align="center">![Outside](./documents/videos/02_left_hand_plays_zoom.webm)</div>

<div align="center">Now the result. Note that I start using the bellow on the very last chord (and thus, producing original sound).</div>

<div align="center"><img src="./documents/article/img/03_magnet_installed.jpg" alt="Magnets" width="500"/></div>

<div align="center">Small Neodim magnets are set with Blu Tack.</div>

<div align="center"><img src="./documents/article/img/02_PCB_installed.jpg" alt="PCB and magnets" width="500"/></div>

<div align="center">In this case, when a key is pressed, the magnet is away from the Hall-effect sensor.</div>

<div align="center"><img src="./documents/article/img/04_full_line.jpg" alt="Complete line of sensors" width="500"/></div>

<div align="center">Here is a complete line of sensors+magnets. The Arduino Mini is glued behind.</div>


## How-to:
If you are interested in making this system for your own instrument, here are the required parts:
 - Left+right hand PCBs (x3), see Gerbers/Drill/position files *BandoMIDI_both_hands_v1-1.zip* ;
 - Components, see Bill of Materials: *bandoMIDI_v1-1_BoM.ods* ;
 - Arduino Micro (x2) ;
 - Small neodim magnets (x72), I used the reference S-02-04-N by Supermagnet (Cylinder Ø 2 mm, heigh 4 mm, N45) ;
 - Blu Tack / Patafix (and yes: it handles the magnets very fine!)
 - Soldering tools + basic components (wires of course).

Please note that smallest components are 0603, to my experience, these are the smallest surface-mounted component one can solder by hand.

As I wanted to do something durable, I eventually turned 2 USB cables into bulkhead adapters. For that I used small circular connectors from Lemo : they are pretty expensive and requires experience to be soldered but are very durable stuff. Here is the reference I used:
 - USB cables, micro-B for Arduino Mini (x2)
 - Lemo socket, EGG.0B.305.CLL, (x2)
 - Lemo straight plug, FGG.0B.305.CLAD52Z, (x2)
 - Lemo collet, GMA.0B.045.DN, (x2)

<div align="center"><img src="./documents/article/img/09_left_hand_playing.jpg" alt="Left hand playing" width="300"/></div>

<div align="center">USB on my 60's chromatic Fratelli-Crosio.</div>


I also did a small stand-alone synthesizer, it gathers an Odroid C1+ and a HiFi shield. This small system runs fluidsynth with a bandoneon soundfont.

## ToDo (improvements)

### On the PCB
 - use connectors between PCBs (JST GH? Adafruit standard?)
 - allow extra inputs
 - improve panelization of right/left hand PCBs.
 - add reference and release on both PCBs!

### Embedded software:
 - runs fine now.

### Other stuff
 - Well I did not made specific cabling schematics. It is quite simple but it would make sens to add to this project.
 - Odroid and fluidsynth runs fine, an adapted case with a volume potentiometer in order to plug headphones directly would be great.

## Links
 - I posted the project on [Hackaday](https://hackaday.io/project/170753-bandomidi "Hackaday").
